from carrera.models import Categoria, Piloto, Auto

def cargar_auto_base(modelo,marca,patente):
    lista_autos = Auto.objects.all()

    #recorrer la lista
    for item in lista_autos:
        if item.patente == int(patente):
            raise Exception("ESTA PATENTE YA EXISTE")

    auto = Auto()
    auto.modelo = modelo
    auto.marca = marca
    auto.patente = patente
    auto.save() #guardamos el auto en nuestra BD

def modifcar_auto_base(auto, modelo, marca, patente):
    lista_autos = Auto.objects.all()

    # recorrer la lista
    for item in lista_autos:
        if item.patente == int(patente):
            raise Exception("ESTA PATENTE YA EXISTE")

    auto.modelo = modelo
    auto.marca = marca
    auto.patente = patente
    auto.save()  # guardamos el auto en nuestra BD
