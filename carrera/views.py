from django.shortcuts import render, redirect
from carrera.models import Categoria, Piloto, Auto
from carrera.help.cargas import cargar_auto_base, modifcar_auto_base
from django.db import IntegrityError

# Create your views here.
def cargar_auto(request):
    lista_autos = Auto.objects.all()
    
    #condicional
    if request.method == 'POST':
        try:
            modelo = request.POST.get('modelo')
            marca = request.POST.get('marca')
            patente = request.POST.get('patente')
            cargar_auto_base(modelo,marca,patente)
        except IntegrityError as ErrDB:
            return render(request,'cargar_auto.html',{'lista_autos':lista_autos,'error':str(ErrDB)})
        
        except Exception as err:
            return render(request, 'cargar_auto.html', {'lista_autos': lista_autos, 'error': str(err)})
    
    return render(request, 'cargar_auto.html',{'lista_autos':lista_autos})

def modificar_auto(request,id_auto):
    auto = Auto.objects.get(id=id_auto)
    lista_autos = Auto.objects.all()
    if request.method == 'POST':
        try:
            modelo = request.POST.get('modelo')
            marca = request.POST.get('marca')
            patente = request.POST.get('patente')
            modifcar_auto_base(auto, modelo, marca, patente)
            
        except IntegrityError as ErrDB:
            return render(request, 'modificar_auto.html', {'auto':auto, 'error': str(ErrDB)})

        except Exception as err:
            return render(request, 'modificar_auto.html', {'auto':auto, 'error': str(err)})

        return redirect('/carrera/cargar_auto')
    
    return render(request,'modificar_auto.html',{'auto':auto, 'lista_autos':lista_autos})

