from django.db import models

# Create your models here.
class Auto(models.Model): # CREAMOS NUESTROS AUTOS
    modelo = models.CharField(max_length=100)
    marca = models.CharField(max_length=100)
    patente = models.IntegerField()

class Piloto(models.Model): # CREAMOS UN PILOTO Y LE ASIGNOS UN AUTO (DE NUESTRA DE AUTOS)
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    dni = models.IntegerField()
    n_auto= models.ForeignKey(Auto, on_delete=models.CASCADE, related_name='auto')

class Categoria(models.Model): # CREAMOS CATEGORIA Y LE ASIGNOS UN PILOTO
    nombre = models.CharField(max_length=100)
    n_piloto = models.ForeignKey(Piloto, on_delete=models.CASCADE, related_name='piloto') #NUESTRA LISTA DE PILOTOS









